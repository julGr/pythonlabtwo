# RMtool 


Console utility for deleting files / directories with support "Recycle Bin" and the ability to restore deleted objects. Interface similar to the system command 'rm' with
additional opportunities.


###List of basic operations:

* Deleting file / directory
* Delete all objects by regular expression in a given subtree
* View the trashbin
* Automatic cleaning by different policies
* Object recovery

The trashbin location and cleaning policy should be specified by the user through configuration file.

### Commands
* "remove" -  delete file or directory in trashbin
* "restore" -  restore file or directory from trashbin
* "inspect" - show trashbin's content 
* "createconfig" - create configuration file.

After the command, you can use one or more arguments

### Arguments

* "-i", "--interactive" - Asks before every action
* "-s", "--silent" - Does not ask before every action
* "--trashbinpath" - Set trashbin path
* "--maxtrashbinsize" - Set max trashbin size
* "--maxcountelem" - Set the maximum allowable amount of elements
* "--timer" - Set the time after which the basket will be cleared
* "--regex" - Do the removal by regex passed.

### Installation
The following steps are required for installation:
```sh
 source ./env/bin/activate
 pip install json
 pip install ./
```
Now you can use argument 'myrm'. 

* **Removing**

>The brackets are shown for clarity

```sh
myrm remove [argument(s)] (path to file(s)/directory(ies))
```
* **Restoring**
```sh
myrm remove [argument(s)] (file name(s) in the trashbin)
```

* **Inspection**
```sh
myrm inspect [argument(s)] -
```
>This command doesn't need any path, but you should text '-'.

* **Create configuration file**

```sh
myrm createconfig [argument(s)] (where config file will be created)
```


