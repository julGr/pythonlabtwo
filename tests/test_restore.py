import unittest
import os
from os.path import (exists, join)
import shutil

import json

from rm_package import remove
from rm_package import restore
from rm_package import trashbin


class Test_Restore(unittest.TestCase):
    def setUp(self):
        self.trash = trashbin.Trashbin(trashbin_path="test_trashbin")
        self.files = ["file1", "file2",  "file3"]
        for file in self.files:
            with open(file, "w") as f:
                f.write("111")
            remove._remove_single_elem(file, self.trash.trashbin_path)

    def test_restore_elems(self):
        restore.restore(self.files, self.trash.trashbin_path)
        for file in self.files:
            self.assertFalse(exists(join(self.trash.trashbin_path, file)))
            self.assertTrue(exists(file))

    def test_restore_one_elem(self):
        restore.restore(self.files[0], self.trash.trashbin_path)
        self.assertFalse(exists(join(self.trash.trashbin_path, self.files[0])))
        self.assertTrue(exists(self.files[0]))

    def test_info_file(self):
        with open(self.trash.info_file_path, 'r') as f:
            file_list1 = json.loads(f.read())
        self.assertEqual(len(file_list1), 3)
        restore.restore(self.files, self.trash.trashbin_path)
        with open(self.trash.info_file_path, 'r') as f:
            file_list2 = json.loads(f.read())
        self.assertEqual(len(file_list2),0)
        with self.assertRaises(AssertionError):
            self.assertEqual(file_list1, file_list2)

    def tearDown(self):
        for file in self.files:
            if exists(file):
                os.remove(file)
        if exists(self.trash.trashbin_path):
            shutil.rmtree(self.trash.trashbin_path)

if __name__ == '__main__':
    unittest.main()
