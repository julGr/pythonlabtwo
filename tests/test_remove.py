import unittest
import os
from os.path import join, exists
import shutil

import json

from rm_package import remove
from rm_package import trashbin
from rm_package import config


class Test_Remove(unittest.TestCase):
    def setUp(self):
        self.trash = trashbin.Trashbin(trashbin_path="test_trashbin")
        self.files = ["file1", "file2",  "file3"]
        for file in self.files:
            with open(file, "w") as f:
                f.write("111")

    def test_remove_one_elem(self):
        for file in self.files:
            remove.remove(file, self.trash.trashbin_path)
            self.assertTrue(exists(join(self.trash.trashbin_path, file)))

    def test_remove_elems(self):
        remove.remove(self.files, self.trash.trashbin_path)
        for file in self.files:
            self.assertTrue(exists(join(self.trash.trashbin_path, file)))

    def test_info_file_creation(self):
        remove.remove(self.files[0], self.trash.trashbin_path)
        with open(self.trash.info_file_path, 'r') as f:
            file_list = json.loads(f.read())
        self.assertEqual(len(file_list), 1)
        self.assertEqual(type(file_list), dict)
        self.assertEqual(file_list.keys()[0], "file1")
        self.assertEqual(len(file_list["file1"]), 3)

    def test_resolve_name_conflict(self):
        remove.remove(self.files, self.trash.trashbin_path)
        with open("file1", "w") as f:
            f.write("111")
        remove.remove("file1", self.trash.trashbin_path)
        self.assertTrue(exists(join(self.trash.trashbin_path, "file1(1)")))
        count = 0
        for file in os.listdir(self.trash.trashbin_path):
            if file == "file1":
                count += 1
        self.assertEqual(count, 1)

    def test_removing_by_policy_trashbin_size(self):
        remove.remove(self.files, self.trash.trashbin_path)
        self.assertEqual(
            type(remove.removing_by_policy(
                config.create_config_file(os.path.dirname(__file__),
                                           trashbin_path=self.trash.trashbin_path, trashbin_size=1))), list)

    def test_removing_by_policy_count_elem(self):
        remove.remove(self.files, self.trash.trashbin_path)
        self.assertEqual(
            type(remove.removing_by_policy(
                config.create_config_file(os.path.dirname(__file__),
                                           trashbin_path=self.trash.trashbin_path, max_count_elem=1))), list)

    def test_removing_by_policy_timer(self):
        remove.remove(self.files, self.trash.trashbin_path)
        self.assertEqual(
            type(remove.removing_by_policy(
                config.create_config_file(os.path.dirname(__file__),
                                           trashbin_path=self.trash.trashbin_path, timer=0))), unicode)

    def tearDown(self):
        for file in self.files:
            if exists(join(self.trash.trashbin_path, file)):
                os.remove(join(self.trash.trashbin_path, file))
                shutil.rmtree(self.trash.trashbin_path)
        if exists('config_file.json'):
            os.remove("config_file.json")
            os.remove("config_file.toml")

if __name__ == '__main__':
    unittest.main()