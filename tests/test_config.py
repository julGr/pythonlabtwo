#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import os
from os.path import (exists, dirname)

from rm_package import config


class Test_Config(unittest.TestCase):

    def test_create_config_file(self):
        dictionary = config.create_config_file(dirname(__file__))
        self.assertEqual(len(dictionary["trashbin settings"]), 2)
        self.assertEqual(len(dictionary["policy settings"]), 3)
        self.assertEqual(len(dictionary["removing settings"]), 3)
        self.assertTrue(exists('config_file.json'))
        self.assertTrue(exists('config_file.toml'))

    def test_load_config_file(self):
        dictionary1 = config.create_config_file(dirname(__file__))
        dictionary2 = config.load_config_file('config_file.json')
        self.assertEqual(len(dictionary2["trashbin settings"]), 2)
        self.assertEqual(len(dictionary2["policy settings"]), 3)
        self.assertEqual(len(dictionary2["removing settings"]), 3)
        self.assertEqual(dictionary1, dictionary2)

    def tearDown(self):
        os.remove('config_file.json')
        os.remove('config_file.toml')

if __name__ == '__main__':
    unittest.main()