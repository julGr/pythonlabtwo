#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import os
import shutil

from rm_package import trashbin
from rm_package import remove


class Test_Trashbin(unittest.TestCase):
    def setUp(self):
        self.trash = trashbin.Trashbin(trashbin_path="test_trashbin")
        self.files = ["file1", "file2",  "file3"]

        for file in self.files:
            with open(file, "w") as f:
                f.write("111")
            remove._remove_single_elem(file, self.trash.trashbin_path)

    def test_creation(self):
        self.assertTrue(os.path.exists(self.trash.trashbin_path))
        self.assertTrue(os.path.exists(self.trash.info_file_path))

    def test_check_size(self):
        with open("new_file", "w") as f:
            f.write("111")
        self.assertTrue(self.trash.check_size("new_file"))

    def test_clean(self):
        self.trash.clean(self.files[0])
        self.assertFalse(os.path.exists(os.path.join(self.trash.trashbin_path, "file1")))

    def tearDown(self):
        if os.path.exists("new_file"):
            os.remove("new_file")
        shutil.rmtree(self.trash.trashbin_path)

if __name__ == '__main__':
    unittest.main()
