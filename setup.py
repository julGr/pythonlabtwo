from os.path import join, dirname
from setuptools import setup, find_packages

setup(
    name="RMtool",
    version=0.01,
    packages=find_packages(),

    long_description=open(join(dirname(__file__), "README.md")).read(),
    entry_points={
        'console_scripts':
            ['myrm = rm_package.app:start']
    },
    package_data = {'': ['config_file.json', 'config_file.toml']},

)
