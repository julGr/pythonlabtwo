import argparse


def parse():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()

    parser.add_argument("command",
                        choices=["remove", "restore", "inspect", "createconfig"],
                        type=str,
                        help="Choose one of these commands ('remove' and 'restore' need the parameter 'path')")
    parser.add_argument("path",
                        nargs="+",
                        type=str,
                        help="Print path to the for removing or restoring file for commands 'remove' or 'restore'. "
                             "Print '-' instead of path for command 'inspect'. "
                             "Print path to your config file for command 'createconfig'. ",
                        default="")
    group.add_argument("-i", "--interactive",
                       action="store_true",
                       help="Asks before every action")
    group.add_argument("-s", "--silent",
                       action="store_true",
                       help="Does not ask before every action")
    parser.add_argument("-d", "--dryrun",
                        action="store_true",
                        help="Simulation of removal")
    parser.add_argument("--trashbinpath",
                        type=str,
                        help="Set trashbin path")
    parser.add_argument("--maxtrashbinsize",
                        type=int,
                        help="Set max trashbin size")
    parser.add_argument("--maxcountelem",
                        type=int,
                        help="Set the maximum allowable amount of elements")
    parser.add_argument("--timer",
                        type=int,
                        help="Set the time after which the basket will be cleared")
    parser.add_argument("--regex",
                        type=str,
                        help="Do the removal by regex passed. ")
    return parser.parse_args()
