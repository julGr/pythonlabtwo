import os
from os.path import join, getsize, basename, expanduser
import logging

import json

from const_values import DEFAULT_TRASHBIN_PATH, DEFAULT_TRASHBIN_SIZE, DEFAULT_MAX_COUNT_ELEM, \
    ERR_EMPTY_INFO_FILE, MSG_INSPECTION_DONE


class Trashbin():
    def __init__(self,
                 trashbin_path=DEFAULT_TRASHBIN_PATH,
                 trashbin_max_size=DEFAULT_TRASHBIN_SIZE,
                 max_count_elem=DEFAULT_MAX_COUNT_ELEM):

        self.trashbin_path = expanduser(trashbin_path)
        self.info_file_path = join(self.trashbin_path, ".info")
        self.trashbin_max_size = trashbin_max_size
        self.max_count_elem = max_count_elem

        if not os.path.exists(self.trashbin_path):
            os.makedirs(self.trashbin_path)
        with open(self.info_file_path, "a+") as f:
            pass

    def get_size(self):
        return getsize(self.trashbin_path)

    def is_empty(self):
        with open(self.info_file_path, 'r') as f:
            try:
                file_list = json.loads(f.read())
            except:
                return True
            if file_list == {}:
                return True
        return False

    def check_size(self, new_removing_file_path):
        if type(new_removing_file_path) is list:
            for file in new_removing_file_path:
                if getsize(self.trashbin_path) + getsize(file) >= self.trashbin_max_size:
                    return False
        else:
            if getsize(self.trashbin_path) + getsize(new_removing_file_path) >= self.trashbin_max_size:
                return False
        return True

    def clean(self, elem=None):
        with open(self.info_file_path, 'r') as f:
            try:
                file_list = json.loads(f.read())
            except ValueError:
                file_list = {}
                logging.error(ERR_EMPTY_INFO_FILE[1])
                exit()

        if type(elem) is not list:
            os.remove(join(self.trashbin_path, elem))
            file_list.pop(os.path.basename(elem))
        else:
            for file_name in elem:
                os.remove(file_name["name_in_trashbin"])
                file_list.pop(os.path.basename(file_name["name_in_trashbin"]))

        with open(self.info_file_path, 'w') as f:
            f.write(json.dumps(file_list))
        logging.info("The cleaning up completed successfully")

    def get_trash_content(self):
        info = []

        with open(self.info_file_path, 'r') as f:
            try:
                list_trash_files = json.loads(f.read())
                if list_trash_files == {}:
                    info.append(ERR_EMPTY_INFO_FILE)
                    return info
            except ValueError:
                info.append(ERR_EMPTY_INFO_FILE)
                return info

            index = 1
            result_data = []

            for elem in list_trash_files:
                if index == 11:
                    break
                print str(index) + ") Name in trashbin: {}\n || Path before removing: {}\n " \
                                   "|| Time of Removing: {}\n".format(
                    basename(list_trash_files[elem]["name_in_trashbin"]),
                    list_trash_files[elem]["name"],
                    list_trash_files[elem]["time_of_removing"])
                index += 1

                result_data.append(os.path.basename(list_trash_files[elem]["name_in_trashbin"]))

        info.append(MSG_INSPECTION_DONE)
        info.append(result_data)

        return info
