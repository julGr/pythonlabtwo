import os

import json
import toml

from const_values import DEFAULT_TRASHBIN_PATH, DEFAULT_TRASHBIN_SIZE, DEFAULT_MAX_COUNT_ELEM, DEFAULT_TIMER, \
    DEFAULT_CONFIG_FILE_PATH


def create_config_file(path_to_config_file,
                       trashbin_path=DEFAULT_TRASHBIN_PATH,
                       trashbin_size=DEFAULT_TRASHBIN_SIZE,
                       max_count_elem=DEFAULT_MAX_COUNT_ELEM,
                       timer=DEFAULT_TIMER,
                       silent_mode=False,
                       interactive_mode=False,
                       dry_run_mode=False):

    dictionary = {
        "trashbin_settings": {
            "trashbin_path": os.path.expanduser(trashbin_path),
            "info_file_path": os.path.join(trashbin_path, "info")
        },
        "policy_settings": {
            "max_trashbin_size": trashbin_size,
            "max_count_elem": max_count_elem,
            "timer": timer
        },
        "removing_settings": {
            "silent_mode": silent_mode,
            "interactive_mode": interactive_mode,
            "dry-run_mode": dry_run_mode

        }
    }

    with open(os.path.join(path_to_config_file, "config_file.json"), "w") as f:
        f.write(json.dumps(dictionary))

    with open(os.path.join(path_to_config_file, "config_file.toml"), "w") as f:
        f.write(toml.dumps(dictionary))

    return dictionary


def load_config_file(path_to_config_file=DEFAULT_CONFIG_FILE_PATH):
    with open(os.path.expanduser(path_to_config_file), "r") as f:
        dictionary = json.loads(f.read())

    return dictionary


def process_arguments(args, dictionary):
    if args.trashbinpath:
        dictionary["trashbin_settings"]["trashbin_path"] = os.path.expanduser(args.trashbinpath)

    if args.maxtrashbinsize:
        dictionary["policy_settings"]["max_trashbin_size"] = args.maxtrashbinsize

    if args.maxcountelem:
        dictionary["policy_settings"]["max_count_elem"] = args.maxcountelem

    if args.timer:
        dictionary["policy_settings"]["timer"] = args.timer

    if args.silent:
        dictionary["removing_settings"]["silent_mode"] = args.silent

    if args.interactive:
        dictionary["removing_settings"]["interactive_mode"] = args.interactive

    if args.dryrun:
        dictionary["removing_settings"]["dry-run_mode"] = args.dryrun

    return dictionary
