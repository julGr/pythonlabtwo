#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from os.path import join
import logging

import remove
import restore
import myargparser
import helper
from helper import ask, select_logger_for_app
from trashbin import Trashbin
import config
from const_values import DEFAULT_CONFIG_FILE_PATH, DEFAULT_CONFIG_FILE_PATH, MSG_RESTORING_DONE, MSG_REMOVING_DONE , \
    ERR_THE_TRASHBIN_IS_OVERFLOW, ERR_NO_SUCH_FILE_OR_DIR


@select_logger_for_app
def start():
    result = ()

    args = myargparser.parse()

    try:
        config_dictionary = config.load_config_file(join(DEFAULT_CONFIG_FILE_PATH, "config_file.json"))
    except IOError:
        config_dictionary = config.create_config_file(DEFAULT_CONFIG_FILE_PATH)

    config_dictionary = config.process_arguments(args, config_dictionary)

    if not config_dictionary["removing_settings"]["silent_mode"]:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.CRITICAL)

    trash = Trashbin(config_dictionary["trashbin_settings"]["trashbin_path"],
                     config_dictionary["policy_settings"]["max_trashbin_size"],
                     config_dictionary["policy_settings"]["max_count_elem"])

    if args.command == "createconfig":
        result = helper.create_config(args.path)

    elif args.command == "restore":
        if args.dryrun is False and config_dictionary["removing_settings"]["dry-run_mode"] is False:
            result = restore.restore(args.path, trash.trashbin_path)
        else:
            result = MSG_RESTORING_DONE

    elem = remove.removing_by_policy(config_dictionary)
    if elem is not None:
        if ask("The trashbin must do the removal by policy. Apply?"):
            trash.clean(elem)

    if args.command == "remove":
        try:
            if trash.check_size(args.path):
                if args.dryrun is False and config_dictionary["removing_settings"]["dry-run_mode"] is False:
                    if args.regex is None:
                        result = remove.remove(args.path, trash.trashbin_path)
                    else:
                        result = remove.regex_remove(args.path, trash.trashbin_path, args.regex)
                else:
                    result = MSG_REMOVING_DONE
            else:
                result = ERR_THE_TRASHBIN_IS_OVERFLOW
        except OSError:
            result = ERR_NO_SUCH_FILE_OR_DIR

    elif args.command == "inspect":
        if args.path[0] != "-":
            print "This command doesn't need a path, but okay."
        result = trash.get_trash_content()[0]

    return result


if __name__ == '__main__':
    start()