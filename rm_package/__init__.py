"""
Console utility for deleting files / directories with support
"Trashbin" and the ability to restore deleted objects

Includes modules:
    app - entry point
    config - works with configurations
    helper - includes method that helps to create config file and method for asking user
    remove - includes methods for removing files and directories
    restore - includes methods for restoring files and directories
    trashbin - trashbin's class

"""