from os.path import expanduser, dirname, join


DEFAULT_TRASHBIN_PATH = expanduser("/home/julia/trashbin")
DEFAULT_INFO_FILE_PATH = expanduser("/home/julia/trashbin/.info")
DEFAULT_CONFIG_FILE_PATH = join(dirname(__file__), 'config_files')
DEFAULT_TRASHBIN_SIZE = 1e9
DEFAULT_MAX_COUNT_ELEM = 100
DEFAULT_TIMER = None

MSG_REMOVING_DONE = (0, "The removal completed")
MSG_RESTORING_DONE = (0, "The restoring completed")
MSG_INSPECTION_DONE = (0, "The inspection of trashbin completed")
MSG_CONFIG_FILES_CREATED = (0, "Config files created")
MSG_THE_CLEANING_UP_DONE = (0, "The cleaning up completed successfully")

ERR_EMPTY_INFO_FILE = (1, "The info file is empty")
ERR_NO_SUCH_FILE_OR_DIR = (1, "No such file or directory")
ERR_PERMISSION_DENIED = (1, "Permission denied")
ERR_THE_TRASHBIN_IS_OVERFLOW = (1, "Can't remove these file: the trashbin max size is reached")
ERR_RESTORING_NAME_CONFLICT = (1, "Can't restore file")
ERR_CANT_FILND_FILES_BY_REGEX = (1, "Can't find files by regex")
ERR_INCORRECT_INPUT_WHILE_CONFIG_CREATING = (1, "This value must have type 'int' ")