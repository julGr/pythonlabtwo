import os
from os.path import expanduser, join, exists

import json

from helper import ask, resolve_name_conflict_while_restoring
from const_values import ERR_EMPTY_INFO_FILE, ERR_RESTORING_NAME_CONFLICT, ERR_NO_SUCH_FILE_OR_DIR, \
    MSG_RESTORING_DONE


def restore(file_names, trashbin_path):

    trashbin_path = expanduser(trashbin_path)

    file_list = _open_info_file(trashbin_path)

    if file_list is None:
        return ERR_EMPTY_INFO_FILE

    if type(file_names) != list:
        success = _restore_single_elem(file_names, trashbin_path, file_list)

    else:
        for file_name in file_names:
            success = _restore_single_elem(file_name, trashbin_path, file_list)

    with open(join(trashbin_path, '.info'), 'w') as f:
            f.write(json.dumps(file_list))

    return success


def _open_info_file(trashbin_path):
    with open(join(trashbin_path, '.info'), 'r') as f:
        try:
            file_list = json.loads(f.read())

            if file_list == {}:
                return None

        except ValueError:
            return None
    return file_list


def _restore_single_elem(file_name, trashbin_path, file_list):
    if exists(join(trashbin_path, file_name)):
        prev_file_path = os.path.dirname(file_list[file_name]["name"])

        if not os.path.exists(file_list[file_name]["name"]):
            os.rename(file_list[file_name]["name_in_trashbin"], file_list[file_name]["name"])
            file_list.pop(file_name)
        else:
            if ask("{} already exist file '{}'. Do you want to create new name?"
                           .format(prev_file_path, file_name)):
                os.rename(join(trashbin_path, file_name), join(prev_file_path, resolve_name_conflict_while_restoring()))
                file_list.pop(file_name)
            else:
                return ERR_RESTORING_NAME_CONFLICT
        return MSG_RESTORING_DONE
    else:
        return ERR_NO_SUCH_FILE_OR_DIR