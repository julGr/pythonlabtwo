import logging
from os.path import expanduser

import config
from const_values import ERR_INCORRECT_INPUT_WHILE_CONFIG_CREATING, MSG_CONFIG_FILES_CREATED


def create_config(config_path):
    config_path = expanduser(config_path[0])

    trashbin_path = expanduser(raw_input("Set trashbin path: "))

    try:
        trashbin_max_size = int(raw_input("Set trashbin max size: "))
        max_count_elem = int(raw_input("Set limit of element in trashbin: "))
        timer = int(raw_input("Set timer in minutes: "))
    except:
        return ERR_INCORRECT_INPUT_WHILE_CONFIG_CREATING

    if ask("Set silent mode? "):
        silent_mode = True
        interactive_mode = False
    else:
        silent_mode = False
        interactive_mode = True

    if ask("Set dry-run mode? "):
        dry_run = True
    else:
        dry_run = False

    config.create_config_file(config_path, trashbin_path, trashbin_max_size, max_count_elem, timer, silent_mode,
                              interactive_mode, dry_run)

    return MSG_CONFIG_FILES_CREATED


def ask(prompt="Are you sure"):
    answer = raw_input(prompt + "Print in console Y/y or N/n:\n")
    if answer.upper() == "Y":
        return True
    elif answer.upper() == "N":
        return False
    else:
        logging.error("Incorrect input")
        exit()


def resolve_name_conflict_while_restoring():
    new_name = raw_input("Create new file name: ")
    return new_name


def select_logger_for_app(func):
    def wrapper():
        result = func()
        if result[0] == 0:
            logging.info(result[1])
        elif result[0] == 1:
            logging.error(result[1])
    return wrapper
